//  ViewController.swift
//  TimeBoxing
//
//  Created by Imthath M on 26/09/18.\

import Cocoa

class TaskViewController: NSViewController, NSWindowDelegate {
    let taskSegmentControl = NSSegmentedControl()
    let optionsSegmentControl = NSSegmentedControl()
    let addButton = NSButton()
    let settingsButton = NSButton()
    let dayTable = DayTable()
    let monthTable = MonthTable()
    let presentLabel = NSTextField(labelWithString: "Today")

    var presentStackView = NSStackView()
    var dayTableView: NSTableView?
    var monthTableView: NSTableView?
    var presentWeek = Date()

    override func viewDidLoad() {
        super.viewDidLoad()

        changeScreenScale()
        initializeDateFormatters()
        addTaskSegments()
        configureOptionSegments()
        addPresentView()
    }

    func changeScreenScale() {
        let screens = NSScreen.screens
        for screen in screens {
            let dict = screen.deviceDescription
            dict.forEach { print("\($0.key) - \($0.value)") }
            print("Screen Scale : \(screen.backingScaleFactor)")
        }
    }

    func initializeDateFormatters() {
        timeFormatter.dateFormat = "HH:mm"
        fullDayFormatter.dateFormat = "MMM d, EEEE"
        dateFormatter.dateFormat = "MMM d"
        weekDayFormatter.dateFormat = "EEEE"
        monthFormatter.dateFormat = "MMMM yyyy"
        dateOnlyFormatter.dateFormat = "d"
    }

    func windowDidResize(_ notification: Notification) {
        if taskSegmentControl.selectedSegment == 2 {
            windowResizes = true
            monthTableView?.reloadData()
        }
    }

    override func viewDidAppear() {
        view.window?.delegate = self
        view.window?.styleMask = [.titled, .miniaturizable, .closable, .resizable]
        view.window?.titleVisibility = .hidden
        let toolbar = NSToolbar(identifier: "toolBar")
        toolbar.delegate = self
        view.window?.toolbar = toolbar
    }
}

// MARK: Task View Segments
extension TaskViewController {
    func addTaskSegments() {
        delegate = self
        taskSegmentControl.segmentCount = 3
        taskSegmentControl.setLabel("Day", forSegment: 0)
        taskSegmentControl.setLabel("Week", forSegment: 1)
        taskSegmentControl.setLabel("Month", forSegment: 2)
        taskSegmentControl.target = self
        taskSegmentControl.action = #selector(showTasksView)
    }

    func addPresentView() {
        presentStackView = configurePresentStackView(presentStackView)
        view.addSubView(taskView, fitTo: .allSides)
        taskSegmentControl.selectedSegment = 0
        showDayView()
    }

    @objc func showTasksView() {
        switch  taskSegmentControl.selectedSegment {
        case 0:
            showDayView()
        case 1:
            showWeekView()
        case 2:
            showMonthView()
        default:
            showDayView()
        }
    }

    @objc func showPrevious() {
        showTaskView(byAdding: -1)
    }

    @objc func showNext() {
        showTaskView(byAdding: 1)
    }

    func showTaskView(byAdding difference: Int) {
        switch taskSegmentControl.selectedSegment {
        case 0:
            presentDay = Calendar.current.date(byAdding: .day, value: difference, to: presentDay)!
        case 1:
            presentWeek = Calendar.current.date(byAdding: .weekOfYear, value: difference, to: presentWeek)!
        case 2:
            presentMonth = Calendar.current.date(byAdding: .month, value: difference, to: presentMonth)!
        default:
            presentDay = Calendar.current.date(byAdding: .day, value: difference, to: presentDay)!
        }
        showTasksView()
    }

    func configurePresentStackView(_ presentStackView: NSStackView) -> NSStackView {
        let previousButton = NSButton(image: NSImage(named: "left")!, target: self, action: #selector(showPrevious))
        let nextButton = NSButton(image: NSImage(named: "right")!, target: self, action: #selector(showNext))
        previousButton.focusRingType = .none
        previousButton.widthAnchor.constraint(equalToConstant: 30.0).isActive = true
        nextButton.widthAnchor.constraint(equalToConstant: 30.0).isActive = true
        presentStackView.setViews([previousButton, presentLabel, nextButton], in: .leading)
        presentLabel.widthAnchor.constraint(equalToConstant: 120.0).isActive = true
        presentLabel.alignment = .center
        return presentStackView
    }
}

// MARK: Taks View Delegate
extension TaskViewController: TaskDelegate {
    func allowFutureMarking(_ sender: NSButton) {
        settings.allowsFutureMarking = settings.allowsFutureMarking.toggle()
        showTasksView()
    }

    func showTaskCount(_ sender: NSButton) {
        settings.showsTasksCountInMonthView = settings.showsTasksCountInMonthView.toggle()
        showTasksView()
    }

    func delete(_ row: Int, withAnimation animates: Bool) {
        let taskId = tasksToDisplay[row].identifier
        if let index = tasks.index(where: {$0.identifier == taskId}) {
            tasks.remove(at: index)
        }
        if animates {
            dayTableView?.removeRows(at: IndexSet(integer: row), withAnimation: .slideLeft)
        }
    }

    @objc func editTask(_ row: Int) {
        let newTaskVC = NewTaskViewController()
        let task = tasksToDisplay[row]
        newTaskVC.editsTask = true
        presentAsModalWindow(newTaskVC)
        newTaskVC.edit(task)
        delete(row, withAnimation: false)
    }

    @objc func deleteTask(_ row: Int) {
        delete(row, withAnimation: true)
    }

    @objc func markTask(_ sender: NSButton) {
        let row = dayTableView?.row(for: sender)
        let taskId = tasksToDisplay[row!].identifier
        if let index = tasks.index(where: {$0.identifier == taskId}) {
            if tasks[index].progress == nil {
                tasks[index].progress = [String: Status]()
            }
            if sender.title == "⭕️"{
                sender.title = "✅"
                tasks[index].progress![fullDayFormatter.string(from: presentDay)] = Status.done
            } else if sender.title == "✅"{
               sender.title = "❌"
               tasks[index].progress![fullDayFormatter.string(from: presentDay)] = Status.failed
            } else {
                sender.title = "⭕️"
                tasks[index].progress![fullDayFormatter.string(from: presentDay)] = Status.noRecord
            }
        }
    }

    func showDayView() {
        taskView.subviews.last?.removeFromSuperview()
        presentLabel.stringValue = fullDayFormatter.string(from: presentDay)
        tasksToDisplay = FileIO.getTasks(for: presentDay)
        if dayTableView == nil { dayTableView = createDayTableView() }
        let dayScrollView = NSScrollView()
        taskView.addSubView(dayScrollView, fitTo: .allSides)
        dayScrollView.documentView = dayTableView
        dayTableView?.reloadData()
    }

    func showWeekView() {
        taskView.subviews.last?.removeFromSuperview()
        presentLabel.stringValue = "\(dateFormatter.string(from: presentWeek.startOfWeek!)) - \(dateFormatter.string(from: presentWeek.endOfWeek!))"
        let weekStackView = ZStackView()
        weekStackView.orientation = .horizontal
        weekStackView.distribution = .fillEqually
        taskView.addSubView(weekStackView, fitTo: .allSides)
        var startDay = presentWeek.startOfWeek!
        for _ in 1...7 {
            let dayStackView = NSStackView()
            dayStackView.orientation = .vertical
            weekStackView.addArrangedSubview(dayStackView)
            dayStackView.translatesAutoresizingMaskIntoConstraints = false
            dayStackView.topAnchor.constraint(equalTo: weekStackView.topAnchor).isActive = true
            dayStackView.widthAnchor.constraint(greaterThanOrEqualToConstant: 70.0).isActive = true

            let dateButton = NSButton()
            dateButton.title = dateFormatter.string(from: startDay)
            dateButton.font = NSFont.boldSystemFont(ofSize: 20.0)
            dateButton.isBordered = false
            dayStackView.addArrangedSubview(dateButton)
            dateButton.translatesAutoresizingMaskIntoConstraints = false
            dateButton.topAnchor.constraint(equalTo: dayStackView.topAnchor).isActive = true

            let weekDayLabel = NSTextField(labelWithString: weekDayFormatter.string(from: startDay))
            dayStackView.addArrangedSubview(weekDayLabel)

            let tasksForTheDay = FileIO.getTasks(for: startDay)
            for task in tasksForTheDay {
                let startTimeLabel = NSTextField(labelWithString: timeFormatter.string(from: task.startTime!))
                let nameLabel = NSTextField(labelWithString: task.name ?? "New Task")
                nameLabel.widthAnchor.constraint(greaterThanOrEqualToConstant: 50.0).isActive = true
                nameLabel.widthAnchor.constraint(lessThanOrEqualToConstant: 120.0).isActive = true

                let taskStackView = NSStackView()
                taskStackView.orientation = .horizontal
                nameLabel.lineBreakMode = .byWordWrapping
                taskStackView.addArrangedSubview(startTimeLabel)
                taskStackView.addArrangedSubview(nameLabel)
                dayStackView.addArrangedSubview(taskStackView)
                taskStackView.translatesAutoresizingMaskIntoConstraints = false
                taskStackView.leadingAnchor.constraint(equalTo: dayStackView.leadingAnchor).isActive = true
            }
            startDay = Calendar.current.date(byAdding: .day, value: 1, to: startDay)!
        }
    }

    func showMonthView() {
        taskView.subviews.last?.removeFromSuperview()
        presentLabel.stringValue = monthFormatter.string(from: presentMonth)

        if monthTableView == nil {
            monthTableView = createMonthTableView()
        }
        let monthScrollView = NSScrollView()
        taskView.addSubView(monthScrollView, fitTo: .allSides)

        let clipView = NSClipView()
        clipView.documentView = monthTableView
        monthScrollView.contentView = clipView

        monthTableView?.translatesAutoresizingMaskIntoConstraints = false
        monthTableView?.topAnchor.constraint(equalTo: clipView.topAnchor).isActive = true
        monthTableView?.leadingAnchor.constraint(equalTo: clipView.leadingAnchor).isActive = true

        monthTable.updateDaysInMonthView()
        windowResizes = false
        monthTableView?.reloadData()
    }

    func createDayTableView() -> NSTableView {
        let column = NSTableColumn(identifier: NSUserInterfaceItemIdentifier("Topics"))
        column.headerCell.title = "Topics"

        let tableView = NSTableView()
        tableView.headerView = nil
        tableView.addTableColumn(column)
        tableView.delegate = dayTable
        tableView.dataSource = dayTable
        tableView.rowHeight = 70.0
        tableView.usesAutomaticRowHeights = false
        tableView.selectionHighlightStyle = .none
        tableView.allowsMultipleSelection = true
        tableView.gridStyleMask = .solidHorizontalGridLineMask
        tableView.gridColor = #colorLiteral(red: 0.921431005, green: 0.9214526415, blue: 0.9214410186, alpha: 1)
        tableView.intercellSpacing = NSSize(width: 5, height: 2)
        return tableView
    }

    func createMonthTableView() -> NSTableView {
        let tableView = NSTableView()
        let weekDays = Calendar.current.weekdaySymbols

        for weekDay in weekDays {
            let column = NSTableColumn(identifier: NSUserInterfaceItemIdentifier(weekDay))
            column.headerCell.title = weekDay
            column.headerCell.alignment = .center
            column.headerCell.font = NSFont.boldSystemFont(ofSize: 20.0)
            column.minWidth = 70.0
            column.headerCell.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            tableView.addTableColumn(column)
        }

        tableView.delegate = monthTable
        tableView.dataSource = monthTable
        tableView.doubleAction = #selector(addTaskPopUp)
        tableView.usesAutomaticRowHeights = false
        tableView.selectionHighlightStyle = .none
        tableView.allowsMultipleSelection = false
        tableView.gridStyleMask = [.solidHorizontalGridLineMask, .solidVerticalGridLineMask]
        tableView.gridColor = #colorLiteral(red: 0.921431005, green: 0.9214526415, blue: 0.9214410186, alpha: 1)
        tableView.intercellSpacing = NSSize(width: 5, height: 5)
        tableView.columnAutoresizingStyle = .uniformColumnAutoresizingStyle
        return tableView
    }

    @objc func addTaskPopUp(_ sender: NSTableView) {
        let row = sender.clickedRow
        let column = sender.clickedColumn
        let cellView = sender.view(atColumn: column, row: row, makeIfNecessary: false) as? MonthCellView ?? MonthCellView()
        let day = Int(cellView.dateButton.title)!
        let month = monthFormatter.date(from: presentLabel.stringValue)!
        let clickedDate = Calendar.current.date(byAdding: .day, value: day-1, to: month)!
        let newTaskVC = NewTaskViewController()
        cellView.showPopOver(using: newTaskVC, withAnimation: false, atEdge: NSRectEdge.maxX)
        newTaskVC.startDatePicker.dateValue = clickedDate
        newTaskVC.endDatePicker.dateValue = clickedDate
        newTaskVC.startTimePicker.dateValue = Calendar.current.date(bySettingHour: 9, minute: 0, second: 0, of: newTaskVC.startTimePicker.minDate!)!
    }
}

// MARK: Option Segments
extension TaskViewController {
    func configureOptionSegments() {
        addButton.image = NSImage(named: "plus")
        addButton.bezelStyle = .rounded
        addButton.imageScaling = .scaleProportionallyDown
        addButton.target = self
        addButton.action = #selector(addButtonAcion)
        addButton.widthAnchor.constraint(equalToConstant: 30.0).isActive = true

        settingsButton.image = NSImage(named: "settings")
        settingsButton.bezelStyle = .rounded
        settingsButton.imageScaling = .scaleProportionallyDown
        settingsButton.target = self
        settingsButton.action = #selector(showSettings)
        settingsButton.widthAnchor.constraint(equalToConstant: 30.0).isActive = true
    }

    @objc func addButtonAcion() {
        changeScreenScale()
        addButton.showPopOver(using: NewTaskViewController())
    }
    @objc func showSettings() {
        settingsButton.showPopOver(using: SettingsViewController())
    }
    @objc func showOptions(segmentController: NSSegmentedControl) {
        switch  segmentController.selectedSegment {
        case 0:
            presentAsModalWindow(NewTaskViewController())
        case 1:
            print("Progress")
        case 2:
            presentAsModalWindow(SettingsViewController())
        default:
            print("default")
        }
        segmentController.selectedSegment = -1
    }
}

// MARK: Toolbar delegate
extension TaskViewController: NSToolbarDelegate {
    func toolbar(_ toolbar: NSToolbar, itemForItemIdentifier itemId: NSToolbarItem.Identifier, willBeInsertedIntoToolbar flag: Bool) -> NSToolbarItem? {
        let toolBarItem = NSToolbarItem(itemIdentifier: itemId)
        if itemId.rawValue == "addButton" {
            toolBarItem.view = addButton
        } else if itemId.rawValue == "settingsButton" {
            toolBarItem.view = settingsButton
        } else if itemId.rawValue == "taskSegment" {
            toolBarItem.view = taskSegmentControl
        } else if itemId.rawValue == "presentStack" {
            toolBarItem.view = presentStackView
        }
        return toolBarItem
    }
    func toolbarAllowedItemIdentifiers(_ toolbar: NSToolbar) -> [NSToolbarItem.Identifier] {
        print("allowed items")
        return [NSToolbarItem.Identifier("taskSegment")]
    }
    func toolbarDefaultItemIdentifiers(_ toolbar: NSToolbar) -> [NSToolbarItem.Identifier] {
        return [ NSToolbarItem.Identifier("taskSegment"),
                .flexibleSpace, NSToolbarItem.Identifier("presentStack"),
                .flexibleSpace, NSToolbarItem.Identifier("addButton"), NSToolbarItem.Identifier("settingsButton")]
    }
}
