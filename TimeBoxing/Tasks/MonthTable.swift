//
//  MonthTable.swift
//  TimeBoxing
//
//  Created by Imthath M on 05/10/18.
//  Copyright © 2018 Imthath M. All rights reserved.
//

import Cocoa

class MonthTable: NSObject, NSTableViewDelegate, NSTableViewDataSource {

    var daysInMonthView = [Date]()
    var weekDays = ["Sunday": 0, "Monday": 1, "Tuesday": 2, "Wednesday": 3, "Thursday": 4, "Friday": 5, "Saturday": 6]

    func tableView(_ tableView: NSTableView, heightOfRow row: Int) -> CGFloat {
        return (taskView.bounds.height-(tableView.headerView?.bounds.height)!)/CGFloat(tableView.numberOfRows)
    }

    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        var tableCellView = tableView.makeView(withIdentifier: (tableColumn?.identifier)!, owner: self) as? MonthCellView

        if tableCellView == nil {
            tableCellView = MonthCellView()
            tableCellView?.identifier = tableColumn?.identifier
        }

        let index = row*7 + weekDays[String((tableColumn?.identifier)!.rawValue)]!
        let dateString = NSAttributedString(string: dateOnlyFormatter.string(from: daysInMonthView[index]), attributes: tableCellView?.attributes)
        tableCellView?.dateButton.attributedTitle = dateString

        var tasksForTheDay = [Task]()
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            tasksForTheDay = FileIO.getTasks(for: self!.daysInMonthView[index])
            DispatchQueue.main.async {
                if tasksForTheDay.count == 0 {
                    tableCellView?.taskStackView.subviews = []
                }
                if !windowResizes {
                    tableCellView?.taskStackView.subviews = []
                    for task in tasksForTheDay {
                        self!.addTasksStack(to: tableCellView, for: task)
                    }
                }

                if !settings.showsTasksCountInMonthView {
                    tableCellView?.tasksCountButton.title = ""
                } else if tasksForTheDay.count > 0 {
                    tableCellView?.tasksCountButton.title = String(tasksForTheDay.count)
                    tableCellView?.tasksCountButton.isBordered = true
                }

            }
        }

        if monthFormatter.string(from: daysInMonthView[index]) != monthFormatter.string(from: presentMonth) {
            tableCellView?.dateButton.isEnabled = false
            tableCellView?.tasksCountButton.isEnabled = false
        }

        return tableCellView
    }

    func numberOfRows(in tableView: NSTableView) -> Int {
        return daysInMonthView.count/7
    }

    func numberOfColumns(in tableView: NSTableView) -> Int {
        return 7
    }

    func updateDaysInMonthView() {
        daysInMonthView = []
        let startOfFirstWeek = presentMonth.startOfMonth.startOfWeek!
        let endOfLastWeek = presentMonth.endOfMonth.endOfWeek!
        var day = startOfFirstWeek
        while day <= endOfLastWeek {
            daysInMonthView.append(day)
            day = Calendar.current.date(byAdding: .day, value: 1, to: day)!
        }
    }

    func addTasksStack(to tableCellView: MonthCellView?, for task: Task) {
        let taskLabel = NSTextField(labelWithString: task.name ?? "New Task")
        taskLabel.font = NSFont.messageFont(ofSize: 12)
        taskLabel.lineBreakMode = .byTruncatingTail

        let timeLabel = NSTextField(labelWithString: timeFormatter.string(from: task.startTime!))
        timeLabel.font = NSFont.messageFont(ofSize: 9)
        timeLabel.textColor = NSColor.gray

        let horizontalSV = NSStackView()
        horizontalSV.orientation = .horizontal

        tableCellView?.taskStackView.addArrangedSubview(horizontalSV)
        horizontalSV.translatesAutoresizingMaskIntoConstraints = false
        horizontalSV.leadingAnchor.constraint(equalTo: (tableCellView?.leadingAnchor)!, constant: 5).isActive = true
        horizontalSV.widthAnchor.constraint(equalTo: (tableCellView?.widthAnchor)!, constant: -5).isActive = true

        horizontalSV.addArrangedSubview(taskLabel)
        taskLabel.translatesAutoresizingMaskIntoConstraints = false
        taskLabel.leadingAnchor.constraint(equalTo: horizontalSV.leadingAnchor).isActive = true
        taskLabel.widthAnchor.constraint(equalTo: horizontalSV.widthAnchor, constant: -32).isActive = true

        horizontalSV.addArrangedSubview(timeLabel)
        timeLabel.translatesAutoresizingMaskIntoConstraints = false
        timeLabel.trailingAnchor.constraint(equalTo: horizontalSV.trailingAnchor).isActive = true
        timeLabel.leadingAnchor.constraint(equalTo: taskLabel.trailingAnchor, constant: 3).isActive = true
    }
}
