//
//  Task.swift
//  TimeBoxing
//
//  Created by Imthath M on 26/09/18.
//  Copyright © 2018 Imthath M. All rights reserved.
//

import Foundation
import Cocoa

struct Task: Codable {
    var identifier: Int?
    var name: String?
    var description: String?
    var category: String?
    var startTime: Date?
    var durationHours: Int?
    var durationMinutes: Int?
    var repeatOn: String?
    var repeatDays: [Int]?
    var fromDate: Date?
    var toDate: Date?
    var progress: [String: Status]?

    init() { }
}

enum Status: String, Codable {
    case done, failed, noRecord
    static var all: [Status] {
        return [.done, .failed, .noRecord]
    }
}

@objc protocol TaskDelegate {
    @objc func showTasksView()
    @objc func editTask(_ row: Int)
    @objc func deleteTask(_ row: Int)
    @objc func markTask(_ sender: NSButton)
    //@objc func goToDate(_ sender: NSButton)
}
