//
//  MonthCellView.swift
//  TimeBoxing
//
//  Created by Imthath M on 05/10/18.
//  Copyright © 2018 Imthath M. All rights reserved.
//

import Cocoa

class MonthCellView: NSTableCellView {
    let dateButton = NSButton()
    let tasksCountButton = NSButton()
    let scrollView = NSScrollView()
    let clipView = NSClipView()
    let taskStackView = ZStackView()
    var attributes = [NSAttributedString.Key: Any]()

    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)

        // Drawing code here.

        dateButton.translatesAutoresizingMaskIntoConstraints = false
        dateButton.topAnchor.constraint(equalTo: self.topAnchor, constant: 5.0).isActive = true
        dateButton.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 5.0).isActive = true

        tasksCountButton.translatesAutoresizingMaskIntoConstraints = false
        tasksCountButton.topAnchor.constraint(equalTo: self.topAnchor, constant: 5.0).isActive = true
        tasksCountButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -5.0).isActive = true
        tasksCountButton.heightAnchor.constraint(equalToConstant: 20.0).isActive = true
        tasksCountButton.widthAnchor.constraint(equalToConstant: 19.0).isActive = true

        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.topAnchor.constraint(equalTo: dateButton.bottomAnchor, constant: 3.0).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5.0).isActive = true

    }

    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        self.addSubview(dateButton)
        self.addSubview(tasksCountButton)
        self.addSubview(scrollView)

        dateButton.font = NSFont.systemFont(ofSize: 15.0)
        dateButton.isBordered = false

        tasksCountButton.title = ""
        tasksCountButton.isEnabled = false
        tasksCountButton.isBordered = false
        tasksCountButton.alignment = .center
        tasksCountButton.bezelStyle = .circular

        clipView.documentView = taskStackView
        scrollView.contentView = clipView
        scrollView.hasVerticalScroller = false

        taskStackView.orientation = .vertical
        taskStackView.spacing = 0.2
        taskStackView.translatesAutoresizingMaskIntoConstraints = false
        taskStackView.topAnchor.constraint(equalTo: clipView.topAnchor).isActive = true
        taskStackView.leadingAnchor.constraint(equalTo: clipView.leadingAnchor).isActive = true
        taskStackView.widthAnchor.constraint(equalTo: clipView.widthAnchor).isActive = true

        attributes[.foregroundColor] = #colorLiteral(red: 0.1298420429, green: 0.1298461258, blue: 0.1298439503, alpha: 1)
    }

    required init?(coder decoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        self.dateButton.isEnabled = true
        self.tasksCountButton.title = ""
        self.tasksCountButton.isEnabled = true
        self.tasksCountButton.isBordered = false
    }
}
