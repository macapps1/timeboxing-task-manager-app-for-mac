//
//  DayTableViewController.swift
//  TimeBoxing
//
//  Created by Imthath M on 04/10/18.
//  Copyright © 2018 Imthath M. All rights reserved.
//

import Cocoa

class DayTable: NSObject, NSTableViewDelegate, NSTableViewDataSource {
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        var tableCellView = tableView.makeView(withIdentifier: (tableColumn?.identifier)!, owner: self) as? DayCellView

        if tableCellView == nil {
            tableCellView = DayCellView()
            tableCellView?.uniqueId = tasksToDisplay[row].identifier
            tableCellView?.identifier = tableColumn?.identifier
        } else {
            tableCellView?.uniqueId = tasksToDisplay[row].identifier
        }

        if tasksToDisplay[row].progress?[fullDayFormatter.string(from: presentDay)] == Status.done {
            tableCellView?.statusCheckBox.title = "✅"
        }

        if !settings.allowsFutureMarking && presentDay > Date() {
            tableCellView?.statusCheckBox.isEnabled = false
            tableCellView?.statusCheckBox.title = ""
        }

        tableCellView?.categoryColorLabel.layer?.backgroundColor = chooseColor(for: tasksToDisplay[row].category ?? "Custom")
        tableCellView?.categoryNameLabel.stringValue = tasksToDisplay[row].category ?? "Custom"
        tableCellView?.startTimeLabel.stringValue = timeFormatter.string(from: tasksToDisplay[row].startTime ?? Date())
        if tasksToDisplay[row].durationHours != nil {
            if tasksToDisplay[row].durationHours! > 0 {
                tableCellView?.descriptionLabel.stringValue = "\(tasksToDisplay[row].durationHours!) hours "
            }
        }
        tableCellView?.descriptionLabel.stringValue += "\(tasksToDisplay[row].durationMinutes!) minutes"
        tableCellView?.nameLabel.stringValue = tasksToDisplay[row].name ?? "New Task"
        return tableCellView
    }

    func numberOfRows(in tableView: NSTableView) -> Int {
        return tasksToDisplay.count
    }

    func tableView(_ tableView: NSTableView, rowActionsForRow row: Int, edge: NSTableView.RowActionEdge) -> [NSTableViewRowAction] {
        if edge == .trailing {
            let rowAction = NSTableViewRowAction(style: .destructive, title: "Delete", handler: { (_, row ) in
                delegate?.deleteTask(row)
            })
            return [rowAction]
        } else {
            let rowAction = NSTableViewRowAction(style: .regular, title: "Edit", handler: { (_, row ) in
                delegate?.editTask(row)
            })
            return [rowAction]
        }
    }

    func chooseColor(for category: String) -> CGColor {
        var color: CGColor
        switch category {
        case "Routine":
            color = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        case "Learn":
            color = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        case "Play":
            color = #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
        case "Work":
            color = #colorLiteral(red: 1, green: 0.5418571234, blue: 0, alpha: 1)
        case "Personal":
            color = #colorLiteral(red: 1, green: 0.2527923882, blue: 1, alpha: 1)
        case "Family":
            color = #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
        default:
            color = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
        }
        return color
    }
}
