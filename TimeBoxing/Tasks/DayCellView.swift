//
//  TaskCellView.swift
//  TimeBoxing
//
//  Created by Imthath M on 30/09/18.
//  Copyright © 2018 Imthath M. All rights reserved.
//

import Cocoa

class DayCellView: NSTableCellView {
    var uniqueId: Int?
    let startTimeLabel = NSTextField(labelWithString: "9:00")
    let nameView = NSView()
    let nameLabel = NSTextField(labelWithString: "Work")
    let descriptionLabel = NSTextField(labelWithString: "")
    let statusCheckBox = NSButton()
    let categoryColorLabel = NSTextField(labelWithString: "")
    let categoryNameLabel = NSTextField(labelWithString: "Custom")

    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)

        // Drawing code here.
        startTimeLabel.translatesAutoresizingMaskIntoConstraints = false
        startTimeLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        startTimeLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        startTimeLabel.widthAnchor.constraint(equalToConstant: 80.0).isActive = true
        startTimeLabel.alignment = .center

        nameView.translatesAutoresizingMaskIntoConstraints = false
        nameView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        nameView.heightAnchor.constraint(equalToConstant: 37.5).isActive = true
        nameView.leadingAnchor.constraint(equalTo: startTimeLabel.trailingAnchor).isActive = true
        nameView.widthAnchor.constraint(equalTo: self.widthAnchor, constant: -145.0).isActive = true

        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.bottomAnchor.constraint(equalTo: nameView.bottomAnchor, constant: -5).isActive = true
        nameLabel.leadingAnchor.constraint(equalTo: nameView.leadingAnchor).isActive = true
        nameLabel.trailingAnchor.constraint(equalTo: nameView.trailingAnchor).isActive = true

        statusCheckBox.translatesAutoresizingMaskIntoConstraints = false
        statusCheckBox.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        statusCheckBox.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10.0).isActive = true

        categoryColorLabel.translatesAutoresizingMaskIntoConstraints = false
        categoryColorLabel.topAnchor.constraint(equalTo: nameView.bottomAnchor, constant: 3.0).isActive = true
        categoryColorLabel.leadingAnchor.constraint(equalTo: descriptionLabel.trailingAnchor, constant: 10).isActive = true
        categoryColorLabel.widthAnchor.constraint(equalToConstant: 10.0).isActive = true
        categoryColorLabel.heightAnchor.constraint(equalToConstant: 13.0).isActive = true

        categoryNameLabel.translatesAutoresizingMaskIntoConstraints = false
        categoryNameLabel.topAnchor.constraint(equalTo: nameView.bottomAnchor).isActive = true
        categoryNameLabel.leadingAnchor.constraint(equalTo: categoryColorLabel.trailingAnchor, constant: 5.0).isActive = true
        categoryNameLabel.widthAnchor.constraint(equalToConstant: 70.0).isActive = true

        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.topAnchor.constraint(equalTo: nameView.bottomAnchor).isActive = true
        descriptionLabel.leadingAnchor.constraint(equalTo: startTimeLabel.trailingAnchor).isActive = true
        descriptionLabel.widthAnchor.constraint(equalToConstant: 150).isActive = true
    }

    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        self.addSubview(startTimeLabel)
        self.addSubview(nameView)
        self.addSubview(descriptionLabel)
        self.addSubview(statusCheckBox)
        self.addSubview(categoryColorLabel)
        self.addSubview(categoryNameLabel)

        nameView.addSubview(nameLabel)
        statusCheckBox.title = "⭕️"
        statusCheckBox.isBordered = false
        statusCheckBox.target = delegate as AnyObject
        statusCheckBox.action = #selector(delegate?.markTask)

        startTimeLabel.font = NSFont.messageFont(ofSize: 22)
        descriptionLabel.font = NSFont.messageFont(ofSize: 15)
        categoryNameLabel.font = NSFont.messageFont(ofSize: 15)
        nameLabel.font = NSFont.messageFont(ofSize: 15)
        nameLabel.lineBreakMode = .byTruncatingTail

        categoryNameLabel.textColor = NSColor.gray
        descriptionLabel.textColor = NSColor.gray
        categoryColorLabel.wantsLayer = true
        categoryColorLabel.layer?.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        categoryColorLabel.layer?.cornerRadius = 3.0
        categoryColorLabel.lineBreakMode = .byTruncatingTail
    }

    required init?(coder decoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.statusCheckBox.title = "⭕️"
        self.descriptionLabel.stringValue = ""
        self.statusCheckBox.isEnabled = true
        self.categoryColorLabel.layer?.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        self.needsDisplay = true
    }
}
