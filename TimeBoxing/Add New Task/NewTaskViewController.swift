//
//  NewTaskViewController.swift
//  TimeBoxing
//
//  Created by Imthath M on 27/09/18.
//  Copyright © 2018 Imthath M. All rights reserved.
//

import Cocoa

class NewTaskViewController: NSViewController {
    let nameField = ZTextField()
    let descriptionField = ZTextField()
    let categoryPopUpButton = NSPopUpButton()
    let startTimePicker = NSDatePicker()
    let minutesPopUpButton = NSPopUpButton()
    let hoursPopUpButton = NSPopUpButton()
    let startDatePicker = ZDatePicker()
    let endDatePicker = ZDatePicker()
    var daysSegmentControl = NSSegmentedControl()
    var editsTask = false
    let repeatPopUpButton = NSPopUpButton()

    let days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
    var selectedDays = [Int]()

    override func loadView() {
        self.view = NSView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        openNewTaskWindow()
//        view.scaleUnitSquare(to: NSSize(width: 0.75, height: 0.75))
    }

    override func viewDidAppear() {
        if !editsTask {
            view.window?.title = "Add Task"
        } else {
            view.window?.title = "Edit Task"
        }
    }
}

// MARK: opening a new Task Window
extension NewTaskViewController {
    func setupView() {
        nameField.placeholderString = "New Task"
        nameField.font = NSFont.systemFont(ofSize: 20.0)
        nameField.focusRingType = .none
        descriptionField.placeholderString = "Description"
        descriptionField.font = NSFont.systemFont(ofSize: 15.0)
        descriptionField.lineBreakMode = .byWordWrapping
        categoryPopUpButton.isBordered = false
        categoryPopUpButton.addItems(withTitles: settings.categories)
        categoryPopUpButton.selectItem(at: 0)
        repeatPopUpButton.addItems(withTitles: ["Week Days", "Week End", "All Days"])
        repeatPopUpButton.isBordered = false
        repeatPopUpButton.target = self
        repeatPopUpButton.action = #selector(repeatsOn)
        configureTimePicker(on: startTimePicker)
        startDatePicker.dateValue = Date().startTime
        endDatePicker.dateValue = Calendar.current.date(byAdding: .day, value: 1, to: Date().startTime)!
        configureDaysSegmentedConrol()
    }

    func openNewTaskWindow() {
        let saveButton = NSButton(title: "Save", target: self, action: #selector(save))
        let cancelButton = NSButton(title: "Cancel", target: self, action: #selector(close))
        let formStackView = NSStackView()
        let durationStackView = createDurationStackView()
        let seperator = ZSeperator()
        let seperator2 = ZSeperator()

        setupView()

        let buttonStackView = NSStackView()
        buttonStackView.orientation = .horizontal
        buttonStackView.addArrangedSubview(saveButton)
        buttonStackView.addArrangedSubview(cancelButton)

        view.addSubView(formStackView, fitTo: .allSidesWithMargin(10.0))
        formStackView.orientation = .vertical
        formStackView.distribution = .equalCentering
        formStackView.spacing = 0.0
        formStackView.addArrangedSubview(nameField)
        formStackView.addArrangedSubview(descriptionField)
        formStackView.addArrangedSubview(seperator)
        formStackView.add(labelWithTitle: "start time", and: startTimePicker)
        formStackView.add(labelWithTitle: "start date", and: startDatePicker)
        formStackView.add(labelWithTitle: "end date", and: endDatePicker)
        formStackView.addArrangedSubview(seperator2)
        formStackView.add(labelWithTitle: "duration", and: durationStackView)
        formStackView.add(labelWithTitle: "repeat on", and: repeatPopUpButton)
        formStackView.addArrangedSubview(buttonStackView)
        formStackView.addSubview(categoryPopUpButton)

        buttonStackView.translatesAutoresizingMaskIntoConstraints = false
        buttonStackView.topAnchor.constraint(equalTo: repeatPopUpButton.bottomAnchor, constant: 10).isActive = true
        buttonStackView.bottomAnchor.constraint(equalTo: formStackView.bottomAnchor).isActive = true
        buttonStackView.heightAnchor.constraint(equalToConstant: 20).isActive = true

        categoryPopUpButton.translatesAutoresizingMaskIntoConstraints = false
        categoryPopUpButton.trailingAnchor.constraint(equalTo: formStackView.trailingAnchor).isActive = true
        categoryPopUpButton.centerYAnchor.constraint(equalTo: nameField.centerYAnchor).isActive = true
        categoryPopUpButton.widthAnchor.constraint(equalToConstant: 80.0).isActive = true

        nameField.translatesAutoresizingMaskIntoConstraints = false
        nameField.trailingAnchor.constraint(equalTo: categoryPopUpButton.leadingAnchor).isActive = true
        nameField.leadingAnchor.constraint(equalTo: formStackView.leadingAnchor).isActive = true
    }

    func createDurationStackView() -> NSStackView {
        let stackView = NSStackView()
        stackView.orientation = .horizontal
        let hoursLabel = NSTextField(labelWithString: "hours")
        let minutesLabel = NSTextField(labelWithString: "minutes")
        minutesPopUpButton.addItems(withTitles: stringArray(of: [Int](stride(from: 5, through: 60, by: 5))))
        minutesPopUpButton.selectItem(at: 5)
        minutesPopUpButton.isBordered = false
        hoursPopUpButton.isBordered = false
        hoursPopUpButton.addItems(withTitles: stringArray(of: [Int](00...12)))
        stackView.setViews([hoursPopUpButton, hoursLabel, minutesPopUpButton, minutesLabel], in: .leading)
        return stackView
    }

    func stringArray(of intArray: [Int]) -> [String] {
        var result = [String]()
        for int in intArray {
            result.append(String(int))
        }
        return result
    }

    func configureTimePicker(on picker: NSDatePicker) {
        let hours = Calendar.current.component(.hour, from: Date())
        let minutes = Calendar.current.component(.minute, from: Date())

        picker.isBordered = false
        picker.datePickerStyle = .textField
        picker.datePickerMode = .single
        picker.calendar = Calendar.current
        picker.minDate = Date(timeIntervalSince1970: 1514745000)
        picker.dateValue = Calendar.current.date(bySettingHour: hours, minute: minutes, second: 0, of: picker.minDate!)!
        picker.maxDate = Date(timeIntervalSince1970: 1514831400)
        picker.datePickerElements = .hourMinute
    }

    @objc func save() {
        var newTask = Task()
        if tasks.count == 0 { newTask.identifier = 1 } else { newTask.identifier = (tasks.last?.identifier)! + 1 }
        newTask.name = nameField.stringValue
        if newTask.name == "" { newTask.name = "New Task" }
        newTask.description = descriptionField.stringValue
        newTask.category = categoryPopUpButton.title
        newTask.startTime = startTimePicker.dateValue
        newTask.durationHours = Int(hoursPopUpButton.title)!
        newTask.durationMinutes = Int(minutesPopUpButton.title)!
        newTask.repeatDays = selectedDays
        newTask.repeatOn = repeatPopUpButton.title
        newTask.fromDate = startDatePicker.dateValue.startTime
        newTask.toDate = endDatePicker.dateValue.startTime
        tasks.append(newTask)
        FileIO.save(tasks, to: "tasks")
        view.window?.close()
        selectedDays = []
        delegate?.showTasksView()
    }

    @objc func close() {
        if editsTask { save() } else {
            view.window?.close()
            selectedDays = []
        }
    }

    func edit(_ task: Task) {
        nameField.stringValue = task.name ?? ""
        descriptionField.stringValue = task.description ?? ""
        categoryPopUpButton.title = task.category!
        startTimePicker.dateValue = task.startTime ?? Date()
        hoursPopUpButton.title = String(task.durationHours ?? 0)
        minutesPopUpButton.title = String(task.durationMinutes ?? 0)
        selectedDays = task.repeatDays ?? []
        repeatPopUpButton.title = task.repeatOn ?? "All Days"
        startDatePicker.dateValue = task.fromDate ?? Date()
        endDatePicker.dateValue = task.toDate ?? Date()
    }
}

// MARK: Choose days to repeat
extension NewTaskViewController {
    func configureDaysSegmentedConrol() {
        daysSegmentControl.segmentCount = days.count
        loadDaysSegment()
        daysSegmentControl.target = self
        daysSegmentControl.action = #selector(dayChosen)
        daysSegmentControl.segmentDistribution = .fillEqually
    }

    func loadDaysSegment() {
        for index in 0..<days.count {
            daysSegmentControl.setLabel("\(days[index]) ◼️", forSegment: index)
        }
    }

    @objc func dayChosen() {
        let index = daysSegmentControl.selectedSegment
        markSegment(with: index)
    }

    func markSegment(with index: Int) {
        if selectedDays.contains(index) {
            daysSegmentControl.setLabel("\(days[index]) ◼️", forSegment: index)
            selectedDays.remove(at: selectedDays.index(of: index)!)
        } else {
            daysSegmentControl.setLabel("\(days[index]) ☑️", forSegment: index)
            selectedDays.append(index)
        }
        daysSegmentControl.selectedSegment = -1
    }

    @objc func repeatsOn(_ sender: NSPopUpButton) {
        selectedDays = []
        loadDaysSegment()
        if sender.title == "Week Days" {
            selectSegments(at: Array(1...5))
        } else if sender.title == "Week End" {
            selectSegments(at: [0, 6])
        } else if sender.title == "All Days" {
            selectSegments(at: Array(0...6))
        }
    }

    func selectSegments(at indices: [Int]) {
        for index in indices {
            markSegment(with: index)
        }
    }
}
