//
//  Depreceted.swift
//  TimeBoxing
//
//  Created by Imthath M on 27/09/18.
//  Copyright © 2018 Zoho Corp. All rights reserved.
//

import Foundation

    // MARK: From main View Controller
//    func addTabBar() {
//        let tabBarStackView = NSStackView()
//        tabBarStackView.orientation = .horizontal
//        tabBarStackView.spacing = 0
//        view.addSubview(tabBarStackView)
//
//        tabBarStackView.translatesAutoresizingMaskIntoConstraints = false
//        tabBarStackView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
//        tabBarStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
//
//        let dayButton = newButton(withLabel: "Day")
//        let weekButton = newButton(withLabel: "Week")
//        let monthButton = newButton(withLabel: "Month")
//        //let progressButton = newButton(withLabel: "Progress")
//        tabBarStackView.setViews([dayButton, weekButton, monthButton], in: .trailing)
//    }
//    func newButton(withLabel title: String) -> NSButton {
//        let button = NSButton()
//        button.title = title
//        button.bezelStyle = .regularSquare
//        //button.isBordered = false
//        button.wantsLayer = true
//        button.layer?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//        button.widthAnchor.constraint(equalToConstant: 50.0).isActive = true
//        return button
//    }
//func displayLabel() {
//    view.addSubview(textLabel)
//    textLabel.translatesAutoresizingMaskIntoConstraints = false
//    textLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
//    textLabel.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
//    textLabel.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
//    textLabel.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
//    textLabel.wantsLayer = true
//    textLabel.alignment = .center
//    textLabel.layer?.backgroundColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
//    textLabel.bezelStyle = .roundedBezel
//}
//    var newTaskWindow: NSWindow!
//    newTaskWindow = NSWindow(contentRect: frame, styleMask: .titled, backing: .buffered, defer: false)
//    let windowSize = newTaskWindow.frame
//    let repeatDays = NSTextField(labelWithString: "Repeat on ")
//    newTaskWindow.title = "New Task"
//    newTaskWindow.makeKeyAndOrderFront(nil)
//    newTaskWindow.contentView? = newTaskView

//    func createRepeatStackView() -> NSStackView {
//        let repeatLabel = NSTextField(labelWithString: "repeats on")
//        let weekDaysButton = NSButton(radioButtonWithTitle: "Week Days", target: self, action: #selector(repeatsOn))
//        let weekEndButton = NSButton(radioButtonWithTitle: "Week End", target: self, action: #selector(repeatsOn))
//        let allDaysButton = NSButton(radioButtonWithTitle: "All Days", target: self, action: #selector(repeatsOn))
//        let repeatOptions = NSPopUpButton()
//        repeatOptions.addItems(withTitles: ["Week Days", "Week End", "All Days", "Custom"])
//        let stackView = NSStackView()
//        stackView.orientation = .horizontal
//        stackView.setViews([repeatLabel, weekDaysButton, weekEndButton, allDaysButton], in: .leading)
//        repeatLabel.translatesAutoresizingMaskIntoConstraints = false
//        repeatLabel.widthAnchor.constraint(equalToConstant: 60.0).isActive = true
//        return stackView
//    }

    // MARK: From Task.swift
//enum Category {
//    case Routine, Learn, Play, Work, Personal, Family
//    case Custom(name: String)
//    static var all: [Category] = [.Routine, .Learn, .Play, .Work, .Personal, .Family]
//    static func add(_ name: String){
//        all.append(Category.Custom(name: name))
//    }
//    static func remove(_ name: String) {
//        let toRemove = all.filter { $0 == Category.Custom(name: name) }
//        if toRemove != nil {
//
//        }
//    }
//}
//class Category {
//    static var all: [String] = ["Routine", "Learn", "Play", "Work", "Personal", "Family"]
//    static func add(_ name: String){
//        all.append(name)
//    }
//    static func remove(_ name: String) {
//        if all.contains(name) {
//            all.remove(at: all.index(of: name)!)
//        }
//    }
//}
//enum Day: Int, Codable {
//    case Sunday = 1, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday
//    static var all: [Day] {
//        return [.Sunday, .Monday, .Tuesday, .Wednesday, .Thursday, .Friday, .Saturday]
//    }
//}

    // MARK: segment control
//        segmentControl.setImage(NSImage(named: "plus"), forSegment: 0)
//        segmentControl.setToolTip("Add new Task", forSegment: 0)
//        segmentControl.setImageScaling(.scaleProportionallyDown, forSegment: 0)
//        segmentControl.setImage(NSImage(named: "progress"), forSegment: 1)
//        segmentControl.setToolTip("View Progress", forSegment: 1)
//        segmentControl.setImageScaling(.scaleProportionallyDown, forSegment: 1)
//        segmentControl.setImage(NSImage(named: "settings"), forSegment: 2)
//        segmentControl.setToolTip("Settings", forSegment: 2)
//        segmentControl.setImageScaling(.scaleProportionallyDown, forSegment: 2)

    // MARK: new window
//        print(newTaskWindow.isResizable)
//        print(newTaskWindow.isModalPanel)
//        print(newTaskWindow.isMiniaturizable)
//        newTaskWindow.showsResizeIndicator = true
//        newTaskWindow.styleMask = .closable
//        newTaskWindow.styleMask = .resizable
//        newTaskWindow.styleMask = .miniaturizable
//        newTaskWindow.setIsMiniaturized(false)
//        newTaskWindow.showsResizeIndicator = false
//        newTaskWindow.contentViewController = newTaskVC
//        newTaskWindow.contentView = newTaskVC.view
//        let newTaskVC = TaskViewController()
//        let newTaskWin = NSWindow(contentViewController: newTaskVC)
//        newTaskWin.makeKeyAndOrderFront(nil)
//        NSApp.runModal(for: newTaskWindow)

//class ZTableView: NSTableView {
//    override func drawGrid(inClipRect clipRect: NSRect) {
//        //super.drawGrid(inClipRect: clipRect)
//        #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1).setStroke()
////        let sep = NSBezierPath(rect: clipRect)
////        sep.stroke()
//
//        let seperatorRect = NSRect(x: self.bounds.minX + 20.0, y: self.bounds.maxY, width: self.bounds.width, height: 0.1)
//        let seperator = NSBezierPath(roundedRect: seperatorRect, xRadius: 0.0, yRadius: 0.0)
//        seperator.stroke()
//
//    }
//}

// MARK: Removing scroll view
//        let clipView = NSClipView()
//        clipView.documentView = dayTableView
//        taskScrollView.contentView = clipView
//        dayTableView?.translatesAutoresizingMaskIntoConstraints = false
//        dayTableView?.leadingAnchor.constraint(equalTo: clipView.leadingAnchor).isActive = true
//        dayTableView?.trailingAnchor.constraint(equalTo: clipView.trailingAnchor).isActive = true

// MARK: Table Header View
//class MonthTableHeaderView: NSTableHeaderView {
//    let headerLabel = NSTextField(labelWithString: "week day")
//    override func draw(_ dirtyRect: NSRect) {
//        super.draw(dirtyRect)
//        
//        // Drawing code here.
//    }
//    
//    override init(frame frameRect: NSRect) {
//        super.init(frame: frameRect)
//        self.addSubView(headerLabel, fitTo: .allSides)
//        headerLabel.alignment = .center
//    }
//    
//    required init?(coder decoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//    
//}
