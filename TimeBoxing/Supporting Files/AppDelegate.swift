//
//  AppDelegate.swift
//  TimeBoxing
//
//  Created by Imthath M on 26/09/18.
//  Copyright © 2018 Imthath M. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application

    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
        FileIO.save(tasks, to: "tasks")
        FileIO.save(settings, to: "settings")
    }

}
