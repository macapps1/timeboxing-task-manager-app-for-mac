//
//  Helper.swift
//  TimeBoxing
//
//  Created by Imthath M on 26/09/18.
//  Copyright © 2018 Imthath M. All rights reserved.
//

import Foundation
import Cocoa

var (settings, tasks) = FileIO.getData()
var delegate: TaskDelegate?
var tasksToDisplay = [Task]()
var presentDay = Date().startTime
var presentMonth = presentDay
var windowResizes = false

let taskView = NSView()
let timeFormatter = DateFormatter()
let fullDayFormatter = DateFormatter()
let dateFormatter = DateFormatter()
let weekDayFormatter = DateFormatter()
let monthFormatter = DateFormatter()
let dateOnlyFormatter = DateFormatter()

class FileIO {
    static func getData() -> (Settings, [Task]) {
        var settings = Settings()
        var tasks = [Task]()
        do {
            let settingssData = read(from: "settings")
            if settingssData != nil {
                settings = try JSONDecoder().decode(Settings.self, from: settingssData!)
            }

            let taskData = read(from: "tasks")
            if taskData != nil {
                tasks = try JSONDecoder().decode([Task].self, from: taskData!)
            }
        } catch let error as NSError {
            print("unable to decode object from text file: \(error.description)")
        }
        return (settings, tasks)
    }

    static func save<T>(_ object: T, to name: String) where T: Codable {
        do {
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            let text = String(data: try encoder.encode(object), encoding: .utf8)!
            try text.write(to: getUrl(of: name), atomically: true, encoding: .utf8)
            print("Saved \(name)")
        } catch let error as NSError {
            print("unable to save: \(error.description)")
        }
    }

    static func read(from name: String) -> Data? {
        var result: Data?
        do {
            result = Data(try String(contentsOf: getUrl(of: name)).utf8)
        } catch let error as NSError {
            print("unable to read: \(error.description)")
        }
        return result
    }

    static func getUrl(of name: String) -> URL {
        var fileUrl: URL?
        do {
            let documentDirectoryUrl = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            fileUrl = documentDirectoryUrl.appendingPathComponent(name).appendingPathExtension("txt")
        } catch let error as NSError {
            print("unable to get file url: \(error.description)")
        }
        return fileUrl!
    }

    static func getTasks(for day: Date) -> [Task] {
        var result = tasks.filter { $0.fromDate! <= day && $0.toDate! >= day && $0.repeatDays!.contains(day.dayOfWeek)}
        result = result.sorted(by: { $0.startTime! <= $1.startTime! })
        return result
    }

}

extension NSSegmentedControl {
    func setImage(_ image: String, at index: Int, withToolTip title: String) {
        self.setImage(NSImage(named: image), forSegment: index)
        self.setToolTip(title, forSegment: index)
        self.setImageScaling(.scaleProportionallyDown, forSegment: index)
    }
}

extension Date {
    var startTime: Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.day, .month, .year], from: self))!
    }

    var startOfWeek: Date? {
        return Calendar.current.date(from: Calendar.current.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))
    }

    var endOfWeek: Date? {
        return Calendar.current.date(byAdding: .day, value: 6, to: self.startOfWeek!)
    }

    var dayOfWeek: Int {
        return Calendar.current.component(.weekday, from: self)-1
    }

    var startOfMonth: Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: self))!
    }

    var endOfMonth: Date {
        let nextMonth = Calendar.current.date(byAdding: .month, value: 1, to: self.startOfMonth)!
        return Calendar.current.date(byAdding: .day, value: -1, to: nextMonth)!
    }
}

class ZStackView: NSStackView {
    override var isFlipped: Bool {  return true }
}

extension NSView {
    func showPopOver(using viewController: NSViewController, withAnimation animates: Bool = false, atEdge edge: NSRectEdge = .minY,
                     behaviour: NSPopover.Behavior = .semitransient) {
        let popOver = NSPopover()
        popOver.contentViewController = viewController
        popOver.behavior = behaviour
        popOver.animates = animates
        let rect = NSRect(x: 0, y: 0, width: 100, height: 100)
        popOver.show(relativeTo: rect, of: self, preferredEdge: edge)
    }

    func addSubView(_ subView: NSView, fitTo corners: ViewFit) {
        self.addSubview(subView)
        subView.translatesAutoresizingMaskIntoConstraints = false
        switch  corners {
        case .allSides:
            subView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
            subView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
            subView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
            subView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        case .allSidesWithMargin(let floatValue):
            subView.topAnchor.constraint(equalTo: self.topAnchor, constant: floatValue).isActive = true
            subView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -floatValue).isActive = true
            subView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: floatValue).isActive = true
            subView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -floatValue).isActive = true
        case .verticalOnly:
            subView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
            subView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        case .horizontalOnly:
            subView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
            subView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        case .topLeading:
            subView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
            subView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        case .topCenter:
            subView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
            subView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        case .topTrailing:
            subView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
            subView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        case .topOnly:
            subView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        }
    }

    enum ViewFit {
        case allSides, verticalOnly, horizontalOnly, topLeading, topCenter, topTrailing, topOnly
        case allSidesWithMargin(CGFloat)
    }
}

extension NSStackView {
    func add(labelWithTitle title: String, and subView: NSView) {
        let horizontalSV = NSStackView()
        let label = NSTextField(labelWithString: title)
        horizontalSV.orientation = .horizontal
        self.addArrangedSubview(horizontalSV)
        horizontalSV.addArrangedSubview(label)
        label.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        label.widthAnchor.constraint(equalToConstant: 60.0).isActive = true
        horizontalSV.addArrangedSubview(subView)
    }
}

extension Bool {
    func toggle() -> Bool {
        if self { return false }
        return true
    }
}

class ZTextField: NSTextField {
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        self.backgroundColor = NSColor.clear
        self.isBordered = false
        self.focusRingType = .none
    }
}

class ZDatePicker: NSDatePicker {
    let calVC = CalendarVC()

    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        self.isBordered = false
        self.datePickerStyle = .textField
        self.datePickerElements = .yearMonthDay
        self.target = self
        self.action = #selector(showCal)
        calVC.datePicker.action = #selector(self.changeSelf)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

//    override func mouseDown(with event: NSEvent) {
//        super.mouseUp(with: event)
//        showCal(self)
//    }

    @objc func showCal(_ sender: NSDatePicker) {
        calVC.datePicker.dateValue = sender.dateValue
        sender.showPopOver(using: calVC, withAnimation: false, atEdge: .minY, behaviour: .transient)
    }

    @objc func changeSelf(_ sender: NSDatePicker) {
        self.dateValue = calVC.datePicker.dateValue
    }
}

class CalendarVC: NSViewController {
    var datePicker = NSDatePicker()

    override func loadView() {
        self.view = NSView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.datePickerElements = .yearMonthDay
        datePicker.datePickerStyle = .clockAndCalendar
        view.addSubView(datePicker, fitTo: .allSides)
    }
}

class ZSeperator: NSTextField {
    override func draw(_ dirtyRect: NSRect) {
        self.isEnabled = false
        #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1).set()
        let rect = NSBezierPath(rect: NSRect(x: self.bounds.minX, y: self.bounds.minY, width: self.bounds.width, height: 0.5))
        rect.stroke()
        self.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }
}
