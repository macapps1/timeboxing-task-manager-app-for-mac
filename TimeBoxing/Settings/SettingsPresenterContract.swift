//
//  SettingsPresenterContract.swift
//  TimeBoxing
//
//  Created by Imthath M on 01/11/18.
//  Copyright © 2018 Imthath M. All rights reserved.
//

import Foundation

protocol SettingPresenterContract {
    func toggleTaskCount()
    func toggleFutureMarking()
}
