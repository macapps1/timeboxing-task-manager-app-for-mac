//
//  SettingsPresenter.swift
//  TimeBoxing
//
//  Created by Imthath M on 01/11/18.
//  Copyright © 2018 Imthath M. All rights reserved.
//

import Foundation

class SettingsPresenter: SettingPresenterContract {
    func toggleTaskCount() {
        settings.showsTasksCountInMonthView = settings.showsTasksCountInMonthView.toggle()
        delegate?.showTasksView()
    }

    func toggleFutureMarking() {
        settings.allowsFutureMarking = settings.allowsFutureMarking.toggle()
        delegate?.showTasksView()
    }
}
