//
//  SettingsViewController.swift
//  TimeBoxing
//
//  Created by Imthath M on 13/10/18.
//  Copyright © 2018 Imthath M. All rights reserved.
//

import Cocoa

class SettingsViewController: NSViewController, NSComboBoxDelegate {
    var presenter: SettingPresenterContract!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        presenter = SettingsPresenter()
        openSettings()
    }

    override func loadView() {
        self.view = NSView()
    }
    override func viewDidAppear() {
        view.window?.title = "Settings"
    }

    func openSettings() {
        let showTasksCountCB = NSButton(checkboxWithTitle: " Show tasks count in month view", target: self, action: #selector(showTaskCount))
        let allowFutureMarkingCB = NSButton(checkboxWithTitle: " Allow future tasks to be marked done", target: self, action: #selector(allowFutureMarking))
        let formStackView = NSStackView()

        view.addSubView(formStackView, fitTo: .allSidesWithMargin(10.0))

        formStackView.orientation = .vertical
        formStackView.spacing = 10.0
        formStackView.alignment = .leading

        formStackView.addArrangedSubview(showTasksCountCB)
        formStackView.addArrangedSubview(allowFutureMarkingCB)

        if settings.showsTasksCountInMonthView { showTasksCountCB.setNextState() }
        if settings.allowsFutureMarking { allowFutureMarkingCB.setNextState() }
        showTasksCountCB.focusRingType = .none
    }

    @objc func showTaskCount(_ sender: NSButton) {
        presenter.toggleTaskCount()
    }

    @objc func allowFutureMarking(_ sender: NSButton) {
        presenter.toggleFutureMarking()
    }
}
