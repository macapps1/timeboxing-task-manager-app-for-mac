//
//  Settings.swift
//  TimeBoxing
//
//  Created by Imthath M on 15/10/18.
//  Copyright © 2018 Imthath M. All rights reserved.
//

import Foundation

class Settings: Codable {
    var showsTasksCountInMonthView = false
    var allowsFutureMarking = false
    var categories = ["Other", "Routine", "Learn", "Play", "Work", "Personal", "Family"]
}
