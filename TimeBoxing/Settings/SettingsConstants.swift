//
//  SettingsConstants.swift
//  TimeBoxing
//
//  Created by Imthath M on 01/11/18.
//  Copyright © 2018 Imthath M. All rights reserved.
//

import Foundation

class SettingsConstants {
    static let tasksCountCheckBoxTitle = " Show tasks count in month view"
    static let futureMarkCheckBoxTitle = " Allow future tasks to be marked done"
}
